// import React, { Component } from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import NavBar from "./components/layout/NavBar";
import Home from "./components/layout/Home";
import backgroundImage from "./assets/background.jpg";
import PokemonDetail from "./components/pokemon/PokemonDetail";

function App() {
  return (
    <Router>
      <div className="App" style={{ background: `url(${backgroundImage})` }}>
        <NavBar />
        <div className="container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route
              exact
              path="/pokemon/:pokemonIndex"
              component={PokemonDetail}
            />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
